import cv2
import numpy as np

import pipeline.detect as detect
import pipeline.detect_light_reflector as detectLight
import angle as angleCalc

cap = cv2.VideoCapture(0)

findSingleObject = detect.DetectObject()
findCenterOf2Object = detectLight.LightReflector()


def detectSingle(img):

    findSingleObject.process(img)

    cnts = findSingleObject.filter_contours_output

    for c in cnts:
        # compute the center of the contour
        M = cv2.moments(c)
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        angle = angleCalc.getXAngle(cX)

        # draw the contour and center of the shape on the image
        cv2.circle(img, (cX, cY), 7, (255, 255, 255), -1)
        cv2.putText(img, str(angle), (cX - 20, cY - 20),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

    cv2.drawContours(img, cnts, -1, (0, 255, 0), 3)


def detect2Objects(img):
    findCenterOf2Object.process(img)

    cnts = findCenterOf2Object.filter_contours_output

    for c in cnts:
        # compute the center of the contour
        M = cv2.moments(c)
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        angle = angleCalc.getXAngle(cX)

        # draw the contour and center of the shape on the image
        cv2.circle(img, (cX, cY), 7, (255, 255, 255), -1)
        cv2.putText(img, str(angle), (cX - 20, cY - 20),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

    if(len(cnts) >= 2):
        M1 = cv2.moments(cnts[0])
        cX1 = int(M1["m10"] / M1["m00"])
        cY1= int(M1["m01"] / M1["m00"])

        M2 = cv2.moments(cnts[1])
        cX2 = int(M2["m10"] / M2["m00"])
        cY2 = int(M2["m01"] / M2["m00"])

        avgX = int((cX1 + cX2)/2)
        avgY = int((cY1 + cY2)/2)

        angle = angleCalc.getXAngle(avgX)


        # draw the contour and center of the shape on the image
        cv2.circle(img, (avgX, avgY), 7, (255, 255, 255), -1)
        cv2.putText(img, str(angle), (avgX - 20, avgY - 20),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)

        cv2.line(img, (cX1, cY1), (cX2, cY2), (255, 255, 0), 5)

    cv2.drawContours(img, cnts, -1, (0, 0, 255), 3)



while True:
    ret, img = cap.read()
    imgclone = img.copy()

    detect2Objects(imgclone)
    detectSingle(imgclone)

    cv2.imshow("img", img)
    cv2.imshow("after", imgclone)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()