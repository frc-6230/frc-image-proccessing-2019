cameraX = 640
cameraY = 480
fovX = 60

fovXPerPixel = (fovX/2)/(cameraX/2)


def getXAngle(pos):
    return fovXPerPixel * (pos - (cameraX/2))